package com.webdevzoo.bhootfmandfmliveonline.utils;


public interface Constants {
    interface Firebase {
        String MP3 = "mp3";
        String FM = "FM";
        String ADS = "ads";
    }

    interface ActivityInteraction {
        String YEAR_ID = "Year id";
        String MONTH_ID = "Month id";
        String FM_ID = "FM id";
    }

    interface MainScreenType {
        String MP3_SCREEN = "Mp3 screen";
        String FM_SCREEN = "FM screen";
    }

    int ADS_SIZE = 2;

    interface PlayView {
        String PLAY = "Play";
        String PAUSE = "Pause";
    }
}
