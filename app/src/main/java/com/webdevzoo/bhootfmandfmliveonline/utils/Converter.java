package com.webdevzoo.bhootfmandfmliveonline.utils;

import com.google.firebase.database.DataSnapshot;
import com.google.gson.Gson;
import com.webdevzoo.bhootfmandfmliveonline.model.Ads;
import com.webdevzoo.bhootfmandfmliveonline.model.ConvertableAds;
import com.webdevzoo.bhootfmandfmliveonline.model.ConvertableFM;
import com.webdevzoo.bhootfmandfmliveonline.model.ConvertableMp3;
import com.webdevzoo.bhootfmandfmliveonline.model.FM;
import com.webdevzoo.bhootfmandfmliveonline.model.Month;
import com.webdevzoo.bhootfmandfmliveonline.model.Mp3;
import com.webdevzoo.bhootfmandfmliveonline.model.Year;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Converter {

    public static ArrayList<Ads> convertDataToAds(DataSnapshot data) {
        ArrayList<ConvertableAds> res = new ArrayList<>();

        try {
            HashMap<Object, Object> value = (HashMap<Object, Object>) data.getValue();

            for (Map.Entry<Object, Object> entry : value.entrySet()) {
                JSONObject jo = new JSONObject((Map) entry.getValue());
                Gson gson = new Gson();
                res.add(gson.fromJson(jo.toString(), ConvertableAds.class));
            }
        } catch (Exception e) {
            for (DataSnapshot jobSnapshot : data.getChildren()) {
                ConvertableAds ads = jobSnapshot.getValue(ConvertableAds.class);
                res.add(ads);
            }
        }

        ArrayList<Ads> ads = new ArrayList<>();

        for (int i = 0; i < res.size(); i++) {
            ConvertableAds element = res.get(i);
            String image = element.image;
            String url = element.url;
            ads.add(new Ads(image, url));
        }

        return ads;
    }

    public static ArrayList<Object> convertDataToYears(DataSnapshot data) {
        ArrayList<ConvertableMp3> res = new ArrayList<>();

        try {
            HashMap<Object, Object> value = (HashMap<Object, Object>) data.getValue();

            for (Map.Entry<Object, Object> entry : value.entrySet()) {
                JSONObject jo = new JSONObject((Map) entry.getValue());
                Gson gson = new Gson();
                res.add(gson.fromJson(jo.toString(), ConvertableMp3.class));
            }
        } catch (Exception e) {
            for (DataSnapshot jobSnapshot : data.getChildren()) {
                ConvertableMp3 mp3 = jobSnapshot.getValue(ConvertableMp3.class);
                res.add(mp3);
            }
        }

        ArrayList<Year> years = new ArrayList<>();

        for (int i = 0; i < res.size(); i++) {
            ConvertableMp3 element = res.get(i);
            String yearString = element.year;
            String monthString = element.month;
            String title = element.title;
            String image = element.image;
            String url = element.file;
            Mp3 mp3 = new Mp3(title, image, url);
            Month month = new Month(monthString);
            month.addMp3(mp3);
            Year year = new Year(yearString);
            year.addMonth(month);
            boolean isYearHere = false;
            for (int j = 0; j < years.size(); j++) {
                if (years.get(j).getTitle().equals(year.getTitle())) {
                    boolean isMonthHere = false;
                    for (int k = 0; k < years.get(j).getMonths().size(); k++) {
                        if (years.get(j).getMonths().get(k).getTitle().equals(month.getTitle())) {
                            years.get(j).getMonths().get(k).addMp3(mp3);
                            isMonthHere = true;
                            break;
                        }
                    }
                    if (!isMonthHere) {
                        years.get(j).addMonth(month);
                    }
                    isYearHere = true;
                    break;
                }
            }
            if (!isYearHere) {
                years.add(year);
            }
        }

        return yearsToObjects(years);
    }

    public static ArrayList<Object> convertDataToFMs(DataSnapshot data) {
        ArrayList<ConvertableFM> res = new ArrayList<>();

        try {
            HashMap<Object, Object> value = (HashMap<Object, Object>) data.getValue();

            for (Map.Entry<Object, Object> entry : value.entrySet()) {
                JSONObject jo = new JSONObject((Map) entry.getValue());
                Gson gson = new Gson();
                res.add(gson.fromJson(jo.toString(), ConvertableFM.class));
            }
        } catch (Exception e) {
            for (DataSnapshot jobSnapshot : data.getChildren()) {
                ConvertableFM fm = jobSnapshot.getValue(ConvertableFM.class);
                res.add(fm);
            }
        }

        ArrayList<FM> fms = new ArrayList<>();

        for (int i = 0; i < res.size(); i++) {
            ConvertableFM element = res.get(i);
            String title = element.title;
            String image = element.image;
            String url = element.file;
            fms.add(new FM(title, image, url));
        }

        return fmsToObjects(fms);
    }

    public static ArrayList<Year> objectsToYears(ArrayList<Object> objects) {
        ArrayList<Year> result = new ArrayList<>();
        for (Object element :
                objects) {
            result.add((Year) element);
        }

        return result;
    }

    public static ArrayList<Object> yearsToObjects(ArrayList<Year> years) {
        ArrayList<Object> result = new ArrayList<>();

        result.addAll(years);

        return result;
    }

    public static ArrayList<FM> objectsToFMs(ArrayList<Object> objects) {
        ArrayList<FM> result = new ArrayList<>();
        for (Object element :
                objects) {
            result.add((FM) element);
        }

        return result;
    }

    public static ArrayList<Object> fmsToObjects(ArrayList<FM> fms) {
        ArrayList<Object> result = new ArrayList<>();

        result.addAll(fms);

        return result;
    }
}
