package com.webdevzoo.bhootfmandfmliveonline.utils.customView;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.presenter.PlaybackPresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayerView extends ConstraintLayout {

    @BindDrawable(R.drawable.ic_play)
    Drawable iconPlay;
    @BindDrawable(R.drawable.ic_pause)
    Drawable iconPause;

    @BindView(R.id.player_cover)
    ImageView cover;
    @BindView(R.id.player_title)
    TextView title;
    @BindView(R.id.player_seekbar)
    SeekBar seekBar;
    @BindView(R.id.player_current_time)
    TextView currentTime;
    @BindView(R.id.player_full_time)
    TextView fullTime;

    @BindView(R.id.player_play)
    ImageButton playButton;

    @OnClick({R.id.player_previous, R.id.player_play, R.id.player_stop, R.id.player_next})
    void onControllerClick(View view) {
        switch (view.getId()) {
            case R.id.player_previous:
                break;
            case R.id.player_play:
                if (mPresenter.isTrackPlaying()) {
                    mPresenter.pause();
                    setState(Constants.PlayView.PAUSE);
                } else {
                    mPresenter.play();
                    setState(Constants.PlayView.PLAY);
                }
                break;
            case R.id.player_stop:
                break;
            case R.id.player_next:
                break;
        }
    }

    PlaybackPresenter mPresenter;

    public PlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setPresenter(PlaybackPresenter presenter){
        mPresenter = presenter;
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void init() {
        inflate(getContext(), R.layout.view_player, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    private Timer timer;

    public void setState(String state){
        switch (state){
            case Constants.PlayView.PLAY:
                playButton.setBackground(iconPause);

                final Handler handler = new Handler();
                final Runnable update = () -> {
                    Log.e("progress", String.valueOf(mPresenter.getPlaybackProgress()));
                    seekBar.setProgress(mPresenter.getPlaybackProgress());
                };

                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(update);
                    }
                }, 100, 100);
                break;
            case Constants.PlayView.PAUSE:
                playButton.setBackground(iconPlay);
                timer.cancel();
                seekBar.setProgress(mPresenter.getPlaybackProgress());
                break;
        }
    }
}
