package com.webdevzoo.bhootfmandfmliveonline.presenter;


import com.webdevzoo.bhootfmandfmliveonline.view.BaseView;

public interface BasePresenter {
    void attachView(BaseView view);
    void detachView();
}
