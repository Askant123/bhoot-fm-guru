package com.webdevzoo.bhootfmandfmliveonline.presenter.main;

import com.webdevzoo.bhootfmandfmliveonline.model.Ads;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenter;

import java.util.ArrayList;

public interface MainPresenter extends BasePresenter{
    void requestData(String screenType);

    ArrayList<Object> getData();

    void setFM(int id);

    void requestAds();

    void initCurrentAds();
    ArrayList<Ads> getCurrentAds();
}
