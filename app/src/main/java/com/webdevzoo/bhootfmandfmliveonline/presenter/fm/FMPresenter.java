package com.webdevzoo.bhootfmandfmliveonline.presenter.fm;

import com.webdevzoo.bhootfmandfmliveonline.model.FM;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.PlaybackPresenter;


public interface FMPresenter extends BasePresenter, PlaybackPresenter {
    FM getFM();
}
