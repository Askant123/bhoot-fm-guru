package com.webdevzoo.bhootfmandfmliveonline.presenter.tracks;

import com.webdevzoo.bhootfmandfmliveonline.model.Mp3;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.PlaybackPresenter;

import java.util.ArrayList;


public interface TracksPresenter extends BasePresenter,PlaybackPresenter {
    void initTracks(int yearId, int monthId);
    ArrayList<Mp3> getTracks();
    void setTrack(int id);

    boolean isTrackPlaying();
}
