package com.webdevzoo.bhootfmandfmliveonline.presenter.fm;

import com.webdevzoo.bhootfmandfmliveonline.manager.DataManager;
import com.webdevzoo.bhootfmandfmliveonline.manager.PlaybackManager;
import com.webdevzoo.bhootfmandfmliveonline.model.FM;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.presenter.PlaybackPresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.utils.Converter;
import com.webdevzoo.bhootfmandfmliveonline.view.fm.FMView;

import javax.inject.Inject;


public class FMPresenterImpl extends PlaybackPresenterImpl implements FMPresenter {

    private DataManager mDataManager;

    private FMView getView() {
        return (FMView) mView;
    }

    @Inject
    public FMPresenterImpl(DataManager dataManager, PlaybackManager playbackManager) {
        mDataManager = dataManager;
        mPlaybackManager = playbackManager;
        mPlaybackManager.pause();
    }

    private FM currentFM;

    @Override
    public void setTrack(int id) {
        currentFM = Converter.objectsToFMs(mDataManager.getData()).get(id);
        mPlaybackManager.initTrack(currentFM.getUrl());
    }

    @Override
    public FM getFM() {
        return currentFM;
    }
}
