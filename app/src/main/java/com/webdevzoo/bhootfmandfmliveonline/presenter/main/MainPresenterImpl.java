package com.webdevzoo.bhootfmandfmliveonline.presenter.main;

import android.util.Log;

import com.webdevzoo.bhootfmandfmliveonline.manager.DataManager;
import com.webdevzoo.bhootfmandfmliveonline.manager.FirebaseManager;
import com.webdevzoo.bhootfmandfmliveonline.model.Ads;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;
import com.webdevzoo.bhootfmandfmliveonline.utils.Converter;
import com.webdevzoo.bhootfmandfmliveonline.view.main.MainView;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Scheduler;

public class MainPresenterImpl extends BasePresenterImpl implements MainPresenter {

    private FirebaseManager mFirebaseManager;
    private DataManager mDataManager;

    private MainView getView() {
        return (MainView) mView;
    }

    @Inject
    public MainPresenterImpl(FirebaseManager firebaseManager, DataManager dataManager) {
        mFirebaseManager = firebaseManager;
        mDataManager = dataManager;
    }

    @Override
    public void requestData(String screenType) {
        String key = "";
        switch (screenType) {
            case Constants.MainScreenType.MP3_SCREEN:
                key = Constants.Firebase.MP3;
                break;
            case Constants.MainScreenType.FM_SCREEN:
                key = Constants.Firebase.FM;
                break;
        }
        if (!key.equals("")) {
            getView().showProgressbar();
            mFirebaseManager.getData(key)
                    .subscribe(dataSnapshot -> {
                        getView().hideProgressbar();
                        if (dataSnapshot.exists()) {
                            switch (screenType) {
                                case Constants.MainScreenType.MP3_SCREEN:
                                    mDataManager.setData(Converter.convertDataToYears(dataSnapshot));
                                    break;
                                case Constants.MainScreenType.FM_SCREEN:
                                    mDataManager.setData(Converter.convertDataToFMs(dataSnapshot));
                                    break;
                            }
                            getView().onDataReceived(mDataManager.getData());
                        } else {
                            getView().onDataError();
                        }
                    }, throwable -> {
                        throwable.printStackTrace();
                        getView().onDataError();
                    });
        }
    }

    @Override
    public ArrayList<Object> getData() {
        return mDataManager.getData();
    }

    @Override
    public void setFM(int id) {
        getView().onFMSet(id);
    }

    private ArrayList<Ads> ads;

    @Override
    public void requestAds() {
        mFirebaseManager.getData(Constants.Firebase.ADS)
                .subscribe(dataSnapshot -> {
                    Log.e("Data", dataSnapshot.toString());
                    if (dataSnapshot.exists()) {
                        ads = Converter.convertDataToAds(dataSnapshot);
                        getView().onAdsReceived();
                    }
                });
    }

    private ArrayList<Ads> currentAds;

    @Override
    public void initCurrentAds() {
        currentAds = new ArrayList<>();

        Random random = new Random();

        int adsSize = ads.size() > Constants.ADS_SIZE ? Constants.ADS_SIZE : ads.size();

        int[] indexes = new int[adsSize];

        indexes[0] = random.nextInt(ads.size());

        for (int i = 1; i < adsSize; i++) {
            int value;
            boolean check;
            do {
                check = true;
                value = random.nextInt(ads.size());
                for (int j = i - 1; j >= 0; j--) {
                    if (indexes[j] == value) {
                        check = false;
                        break;
                    }
                }
            } while (!check);
            indexes[i] = value;
        }

        for (int i = 0; i < adsSize; i++) {
            currentAds.add(ads.get(indexes[i]));
        }
    }

    @Override
    public ArrayList<Ads> getCurrentAds() {
        return currentAds;
    }
}
