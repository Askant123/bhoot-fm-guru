package com.webdevzoo.bhootfmandfmliveonline.presenter.tracks;

import android.util.Log;

import com.webdevzoo.bhootfmandfmliveonline.manager.DataManager;
import com.webdevzoo.bhootfmandfmliveonline.manager.PlaybackManager;
import com.webdevzoo.bhootfmandfmliveonline.model.Mp3;
import com.webdevzoo.bhootfmandfmliveonline.presenter.BasePresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.presenter.PlaybackPresenterImpl;
import com.webdevzoo.bhootfmandfmliveonline.utils.Converter;
import com.webdevzoo.bhootfmandfmliveonline.view.tracks.TracksView;

import java.util.ArrayList;

import javax.inject.Inject;


public class TracksPresenterImpl extends PlaybackPresenterImpl implements TracksPresenter {

    private TracksView getView() {
        return (TracksView) mView;
    }

    private DataManager mDataManager;

    @Inject
    public TracksPresenterImpl(DataManager dataManager, PlaybackManager playbackManager) {
        mDataManager = dataManager;
        mPlaybackManager = playbackManager;
        mPlaybackManager.pause();
        tracks = new ArrayList<>();
    }

    private ArrayList<Mp3> tracks;
    private int currentId = 0;

    @Override
    public void initTracks(int yearId, int monthId) {
        tracks = Converter.objectsToYears(mDataManager.getData()).get(yearId).getMonths().get(monthId).getMp3s();
        Log.e("Track size", String.valueOf(tracks.size()));
    }

    @Override
    public ArrayList<Mp3> getTracks() {
        Log.e("Get tracks", "happens");
        return tracks;
    }

    @Override
    public void setTrack(int id) {
        mPlaybackManager.initTrack(tracks.get(id).getUrl());
        currentId = id;
        getView().onTrackSet();
        play();
    }
}
