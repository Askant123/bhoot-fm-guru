package com.webdevzoo.bhootfmandfmliveonline.presenter;


import com.webdevzoo.bhootfmandfmliveonline.view.BaseView;

public class BasePresenterImpl implements BasePresenter {

    protected BaseView mView;

    @Override
    public void attachView(BaseView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
