package com.webdevzoo.bhootfmandfmliveonline.presenter;


import com.webdevzoo.bhootfmandfmliveonline.manager.PlaybackManager;

public abstract class PlaybackPresenterImpl extends BasePresenterImpl implements PlaybackPresenter {
    protected PlaybackManager mPlaybackManager;

    @Override
    public void play() {
        mPlaybackManager.play();
    }

    @Override
    public void pause() {
        mPlaybackManager.pause();
    }

    @Override
    public boolean isTrackPlaying() {
        return mPlaybackManager.isTrackPlaying();
    }

    @Override
    public int getPlaybackProgress() {
        return (int) (mPlaybackManager.getCurrentPosition() * 1000f / mPlaybackManager.getTrackDuration());
    }
}
