package com.webdevzoo.bhootfmandfmliveonline.presenter;


public interface PlaybackPresenter {
    void play();
    void pause();
    void setTrack(int id);
    boolean isTrackPlaying();
    int getPlaybackProgress();
}
