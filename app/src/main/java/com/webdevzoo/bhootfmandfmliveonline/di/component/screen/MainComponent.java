package com.webdevzoo.bhootfmandfmliveonline.di.component.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.component.AppComponent;
import com.webdevzoo.bhootfmandfmliveonline.di.module.screen.MainModule;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.view.main.MainActivity;
import com.webdevzoo.bhootfmandfmliveonline.view.main.fragment.FMFragment;
import com.webdevzoo.bhootfmandfmliveonline.view.main.fragment.Mp3Fragment;

import dagger.Component;

@ActivityScope
@Component(modules = {MainModule.class}, dependencies = {AppComponent.class})
public interface MainComponent {
    void inject(MainActivity activity);

    void inject(Mp3Fragment fragment);

    void inject(FMFragment fragment);
}