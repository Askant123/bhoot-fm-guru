package com.webdevzoo.bhootfmandfmliveonline.di.module.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.presenter.fm.FMPresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.fm.FMPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FMModule {
    @Binds
    @ActivityScope
    public abstract FMPresenter bindFMPresenter(FMPresenterImpl fmPresenter);
}
