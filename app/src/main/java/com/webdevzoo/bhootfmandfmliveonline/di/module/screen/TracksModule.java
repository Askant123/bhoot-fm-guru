package com.webdevzoo.bhootfmandfmliveonline.di.module.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.presenter.tracks.TracksPresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.tracks.TracksPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class TracksModule {
    @Binds
    @ActivityScope
    public abstract TracksPresenter bindTracksPresenter(TracksPresenterImpl tracksPresenter);
}
