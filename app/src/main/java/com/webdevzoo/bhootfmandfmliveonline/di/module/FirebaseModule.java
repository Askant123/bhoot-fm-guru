package com.webdevzoo.bhootfmandfmliveonline.di.module;

import com.webdevzoo.bhootfmandfmliveonline.di.scope.AppScope;
import com.webdevzoo.bhootfmandfmliveonline.manager.FirebaseManager;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseModule {

    @Provides
    @AppScope
    FirebaseManager provideFirebaseManager(){
        return new FirebaseManager();
    }
}
