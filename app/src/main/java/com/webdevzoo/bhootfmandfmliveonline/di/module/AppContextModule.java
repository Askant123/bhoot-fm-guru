package com.webdevzoo.bhootfmandfmliveonline.di.module;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {
    private App mApplication;

    public AppContextModule(App application) {
        mApplication = application;
    }

    @Provides
    @AppScope
    App providesApplication() {
        return mApplication;
    }
}
