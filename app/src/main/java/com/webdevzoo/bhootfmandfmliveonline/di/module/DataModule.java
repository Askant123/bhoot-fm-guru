package com.webdevzoo.bhootfmandfmliveonline.di.module;

import com.webdevzoo.bhootfmandfmliveonline.di.scope.AppScope;
import com.webdevzoo.bhootfmandfmliveonline.manager.DataManager;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @AppScope
    DataManager provideDataManager() {
        return new DataManager();
    }
}
