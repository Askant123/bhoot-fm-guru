package com.webdevzoo.bhootfmandfmliveonline.di.component.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.component.AppComponent;
import com.webdevzoo.bhootfmandfmliveonline.di.module.screen.FMModule;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.view.fm.FMActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {FMModule.class}, dependencies = {AppComponent.class})
public interface FMComponent {
    void inject(FMActivity activity);
}
