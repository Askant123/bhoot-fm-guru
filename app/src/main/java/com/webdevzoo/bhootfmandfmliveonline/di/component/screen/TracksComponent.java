package com.webdevzoo.bhootfmandfmliveonline.di.component.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.component.AppComponent;
import com.webdevzoo.bhootfmandfmliveonline.di.module.screen.TracksModule;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.view.tracks.TracksActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {TracksModule.class}, dependencies = {AppComponent.class})
public interface TracksComponent {
    void inject(TracksActivity activity);
}
