package com.webdevzoo.bhootfmandfmliveonline.di.module;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.AppScope;
import com.webdevzoo.bhootfmandfmliveonline.manager.PlaybackManager;

import dagger.Module;
import dagger.Provides;

@Module
public class PlaybackModule {

    @Provides
    @AppScope
    PlaybackManager providePlaybackManager(App app){
        return new PlaybackManager(app);
    }

}
