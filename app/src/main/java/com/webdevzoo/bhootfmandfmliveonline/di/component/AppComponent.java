package com.webdevzoo.bhootfmandfmliveonline.di.component;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.di.module.AppContextModule;
import com.webdevzoo.bhootfmandfmliveonline.di.module.DataModule;
import com.webdevzoo.bhootfmandfmliveonline.di.module.FirebaseModule;
import com.webdevzoo.bhootfmandfmliveonline.di.module.PlaybackModule;
import com.webdevzoo.bhootfmandfmliveonline.di.scope.AppScope;
import com.webdevzoo.bhootfmandfmliveonline.manager.DataManager;
import com.webdevzoo.bhootfmandfmliveonline.manager.FirebaseManager;
import com.webdevzoo.bhootfmandfmliveonline.manager.PlaybackManager;

import dagger.Component;

@AppScope
@Component(modules = {PlaybackModule.class, DataModule.class,
        FirebaseModule.class, AppContextModule.class})
public interface AppComponent {

    PlaybackManager playback();

    DataManager data();

    FirebaseManager firebase();

    App context();

}
