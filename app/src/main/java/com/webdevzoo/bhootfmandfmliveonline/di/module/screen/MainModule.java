package com.webdevzoo.bhootfmandfmliveonline.di.module.screen;

import com.webdevzoo.bhootfmandfmliveonline.di.scope.ActivityScope;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {
    @Binds
    @ActivityScope
    public abstract MainPresenter bindMainPresenter(MainPresenterImpl mainPresenter);
}
