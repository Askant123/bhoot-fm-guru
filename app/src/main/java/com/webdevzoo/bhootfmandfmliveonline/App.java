package com.webdevzoo.bhootfmandfmliveonline;

import android.app.Application;

import com.webdevzoo.bhootfmandfmliveonline.di.component.AppComponent;
import com.webdevzoo.bhootfmandfmliveonline.di.component.DaggerAppComponent;
import com.webdevzoo.bhootfmandfmliveonline.di.module.AppContextModule;


public class App extends Application {

    private static AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(this))
                .build();
    }

    public static AppComponent getAppComponent(){
        return mAppComponent;
    }
}
