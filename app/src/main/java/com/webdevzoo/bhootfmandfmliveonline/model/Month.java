package com.webdevzoo.bhootfmandfmliveonline.model;


import java.util.ArrayList;

public class Month {
    private String title;
    private ArrayList<Mp3> mp3s;

    public Month(String title){
        this.title = title;
        mp3s = new ArrayList<>();
    }

    public void addMp3(Mp3 mp3){
        mp3s.add(mp3);
    }

    public ArrayList<Mp3> getMp3s() {
        return mp3s;
    }

    public void setMp3s(ArrayList<Mp3> mp3s) {
        this.mp3s = mp3s;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
