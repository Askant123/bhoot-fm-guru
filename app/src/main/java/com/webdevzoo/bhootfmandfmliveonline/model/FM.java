package com.webdevzoo.bhootfmandfmliveonline.model;


public class FM {
    private String title;
    private String image;
    private String url;

    public FM(String title, String image, String url) {
        this.title = title;
        this.image = image;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }
}
