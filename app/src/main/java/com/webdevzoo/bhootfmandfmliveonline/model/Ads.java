package com.webdevzoo.bhootfmandfmliveonline.model;


public class Ads {
    private String image;
    private String url;

    public Ads(String image, String url) {
        this.image = image;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getImage() {
        return image;
    }
}
