package com.webdevzoo.bhootfmandfmliveonline.model;

public class Mp3 {
    private String title;
    private String image;
    private String url;

    public Mp3(String title, String image, String url) {
        this.title = title;
        this.image = image;
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
