package com.webdevzoo.bhootfmandfmliveonline.model;


import java.util.ArrayList;

public class Year {
    private String title;
    private ArrayList<Month> months;

    public Year(String title){
        this.title = title;
        months = new ArrayList<>();
    }

    public void addMonth(Month month){
        months.add(month);
    }

    public ArrayList<Month> getMonths() {
        return months;
    }

    public void setMonths(ArrayList<Month> months) {
        this.months = months;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
