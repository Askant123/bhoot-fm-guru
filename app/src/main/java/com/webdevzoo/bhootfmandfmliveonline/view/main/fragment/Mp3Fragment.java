package com.webdevzoo.bhootfmandfmliveonline.view.main.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.model.Year;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;
import com.webdevzoo.bhootfmandfmliveonline.view.main.adapter.ExpandableListAdapter;
import com.webdevzoo.bhootfmandfmliveonline.view.tracks.TracksActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class Mp3Fragment extends MainFragment {

    private Unbinder unbinder;

    ExpandableListAdapter listAdapter;

    @BindView(R.id.categories_list)
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mp3, container, false);
        unbinder = ButterKnife.bind(this, view);

        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener((expandableListView, expView, i, i1, l) -> {
            Intent intent = new Intent(getActivity(), TracksActivity.class);
            intent.putExtra(Constants.ActivityInteraction.YEAR_ID, i);
            intent.putExtra(Constants.ActivityInteraction.MONTH_ID, i1);
            startActivity(intent);
            return false;
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void onDataReceived(ArrayList<Year> data){
        int yearsLength = data.size();
        for (int i = 0; i < yearsLength; i++) {
            listDataHeader.add(data.get(i).getTitle());
            ArrayList<String> months = new ArrayList<>();
            int monthLength = data.get(i).getMonths().size();
            for (int j = 0; j < monthLength; j++) {
                months.add(data.get(i).getMonths().get(j).getTitle());
            }
            listDataChild.put(data.get(i).getTitle(), months);
        }
        listAdapter.notifyDataSetChanged();
    }
}
