package com.webdevzoo.bhootfmandfmliveonline.view.tracks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.di.component.screen.DaggerTracksComponent;
import com.webdevzoo.bhootfmandfmliveonline.presenter.tracks.TracksPresenter;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;
import com.webdevzoo.bhootfmandfmliveonline.utils.customView.PlayerView;
import com.webdevzoo.bhootfmandfmliveonline.view.BaseActivity;
import com.webdevzoo.bhootfmandfmliveonline.view.BaseView;
import com.webdevzoo.bhootfmandfmliveonline.view.tracks.adapter.Mp3Adapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class TracksActivity extends BaseActivity implements TracksView {

    private Unbinder unbinder;

    @Inject
    TracksPresenter mPresenter;

    private Mp3Adapter mAdapter;

    @BindView(R.id.track_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.player_view)
    PlayerView playerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks);
        unbinder = ButterKnife.bind(this);

        DaggerTracksComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this);
        mPresenter.attachView(this);

        playerView.setPresenter(mPresenter);

        mPresenter.initTracks(getIntent().getIntExtra(Constants.ActivityInteraction.YEAR_ID, 0),
                getIntent().getIntExtra(Constants.ActivityInteraction.MONTH_ID, 0));

        mAdapter = new Mp3Adapter(mPresenter);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        unbinder.unbind();
    }

    @Override
    public void onTrackSet() {
        playerView.setState(Constants.PlayView.PLAY);
        playerView.setVisibility(View.VISIBLE);
    }
}
