package com.webdevzoo.bhootfmandfmliveonline.view.tracks;

import com.webdevzoo.bhootfmandfmliveonline.view.BaseView;


public interface TracksView extends BaseView {
    void onTrackSet();
}
