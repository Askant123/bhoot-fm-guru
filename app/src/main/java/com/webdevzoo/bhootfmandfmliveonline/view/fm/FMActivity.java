package com.webdevzoo.bhootfmandfmliveonline.view.fm;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.di.component.screen.DaggerFMComponent;
import com.webdevzoo.bhootfmandfmliveonline.presenter.fm.FMPresenter;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;
import com.webdevzoo.bhootfmandfmliveonline.view.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FMActivity extends BaseActivity implements FMView {

    private Unbinder unbinder;

    @Inject
    FMPresenter mPresenter;

    @BindView(R.id.play_button)
    Button playButton;

    @OnClick(R.id.play_button)
    public void onPlayClick() {
        if (!mPresenter.isTrackPlaying()) {
            mPresenter.play();
            playButton.setText("Pause");
        } else {
            mPresenter.pause();
            playButton.setText("Play");
        }
    }

    @BindView(R.id.cover)
    ImageView cover;

    @BindView(R.id.title)
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm);

        unbinder = ButterKnife.bind(this);

        DaggerFMComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this);

        mPresenter.attachView(this);

        mPresenter.setTrack(getIntent().getIntExtra(Constants.ActivityInteraction.FM_ID, 0));

        Picasso.with(this)
                .load(Uri.parse(mPresenter.getFM().getImage()))
                .into(cover);
        title.setText(mPresenter.getFM().getTitle());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        unbinder.unbind();
    }
}
