package com.webdevzoo.bhootfmandfmliveonline.view.tracks.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.presenter.tracks.TracksPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Mp3Adapter extends RecyclerView.Adapter<Mp3Adapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cover)
        ImageView cover;
        @BindView(R.id.title)
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private TracksPresenter mPresenter;

    public Mp3Adapter(TracksPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_track_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(holder.cover.getContext())
                .load(Uri.parse(mPresenter.getTracks().get(position).getImage()))
                .into(holder.cover);
        holder.title.setText(mPresenter.getTracks().get(position).getTitle());
        holder.cover.setOnClickListener((view -> mPresenter.setTrack(position)));
    }

    @Override
    public int getItemCount() {
        return mPresenter.getTracks().size();
    }

}
