package com.webdevzoo.bhootfmandfmliveonline.view.main;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.di.component.screen.DaggerMainComponent;
import com.webdevzoo.bhootfmandfmliveonline.model.Ads;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;
import com.webdevzoo.bhootfmandfmliveonline.utils.Constants;
import com.webdevzoo.bhootfmandfmliveonline.utils.Converter;
import com.webdevzoo.bhootfmandfmliveonline.view.BaseActivity;
import com.webdevzoo.bhootfmandfmliveonline.view.fm.FMActivity;
import com.webdevzoo.bhootfmandfmliveonline.view.main.adapter.SliderPagerAdapter;
import com.webdevzoo.bhootfmandfmliveonline.view.main.fragment.FMFragment;
import com.webdevzoo.bhootfmandfmliveonline.view.main.fragment.MainFragment;
import com.webdevzoo.bhootfmandfmliveonline.view.main.fragment.Mp3Fragment;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;

public class MainActivity extends BaseActivity implements MainView,
        NavigationView.OnNavigationItemSelectedListener {

    private Unbinder unbinder;

    @BindView(R.id.ads_block)
    RelativeLayout adsBlock;
    @BindView(R.id.vp_slider)
    ViewPager mSlider;
    @BindView(R.id.ll_dots)
    LinearLayout mDots;
    SliderPagerAdapter sliderPagerAdapter;
    ArrayList<String> sliderImageList;
    private TextView[] dots;
    int pagePosition = 0;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @Inject
    MainPresenter mPresenter;

    private MainFragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        DaggerMainComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this);

        mPresenter.attachView(this);

        mPresenter.requestAds();

        setFragment(Constants.MainScreenType.MP3_SCREEN);

        navigationView.setNavigationItemSelectedListener(this);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
            actionBar.setIcon(android.R.color.transparent);
        }
    }

    private void setFragment(String newScreenType) {
        switch (newScreenType) {
            case Constants.MainScreenType.MP3_SCREEN:
                currentFragment = new Mp3Fragment();
                break;
            case Constants.MainScreenType.FM_SCREEN:
                currentFragment = new FMFragment();
                break;
        }
        currentFragment.setPresenter(mPresenter);
        getSupportFragmentManager().beginTransaction().replace(R.id.content, currentFragment).commit();
        mPresenter.requestData(newScreenType);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mp3) {
            setFragment(Constants.MainScreenType.MP3_SCREEN);
        } else if (id == R.id.nav_fm) {
            setFragment(Constants.MainScreenType.FM_SCREEN);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        unbinder.unbind();
    }

    @Override
    public void onDataReceived(ArrayList<Object> data) {
        if (currentFragment instanceof Mp3Fragment) {
            ((Mp3Fragment) currentFragment).onDataReceived(Converter.objectsToYears(data));
        } else if (currentFragment instanceof FMFragment) {
            ((FMFragment) currentFragment).onDataReceived(Converter.objectsToFMs(data));
        }
    }

    @Override
    public void onDataError() {

    }

    @Override
    public void onFMSet(int id) {
        Intent intent = new Intent(this, FMActivity.class);
        intent.putExtra(Constants.ActivityInteraction.FM_ID, id);
        startActivity(intent);
    }

    @Override
    public void onAdsReceived() {
        Log.e("On ads received", "happens");

        mPresenter.initCurrentAds();

        sliderImageList = new ArrayList<>();

        Observable.fromIterable(mPresenter.getCurrentAds())
                .map(Ads::getImage)
                .subscribe(element -> sliderImageList.add(element));

        sliderPagerAdapter = new SliderPagerAdapter(this, sliderImageList);
        mSlider.setAdapter(sliderPagerAdapter);

        mSlider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        addBottomDots(0);

        if (sliderImageList.size() > 0) {
            adsBlock.setVisibility(View.VISIBLE);
        }

        final Handler handler = new Handler();

        final Runnable update = () -> {
            if (pagePosition == sliderImageList.size()) {
                pagePosition = 0;
            } else {
                pagePosition = pagePosition + 1;
            }
            mSlider.setCurrentItem(pagePosition, true);
        };

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 5000, 5000);

        Log.e("Init", "finish");
    }

    private void addBottomDots(int pagePosition) {
        dots = new TextView[sliderImageList.size()];

        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, r.getDisplayMetrics());

        mDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setAlpha(0.5f);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(40);
            dots[i].setTextColor(Color.parseColor("#FFFFFF"));
            mDots.addView(dots[i]);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) dots[i].getLayoutParams();
            params.setMargins((int) px, 0, (int) px, 0);
            dots[i].setLayoutParams(params);
        }

        if (dots.length > 0) {
            dots[pagePosition].setAlpha(0.8f);
            dots[pagePosition].setTextColor(Color.parseColor("#FFFFFF"));
        }
    }
}
