package com.webdevzoo.bhootfmandfmliveonline.view.main.fragment;

import android.support.v4.app.Fragment;

import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;

public abstract class MainFragment extends Fragment implements MainFragmentView {

    protected MainPresenter mPresenter;

    public void setPresenter(MainPresenter presenter){
        mPresenter = presenter;
    }

}
