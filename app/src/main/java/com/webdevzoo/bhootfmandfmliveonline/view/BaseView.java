package com.webdevzoo.bhootfmandfmliveonline.view;


public interface BaseView {
    void showProgressbar();
    void hideProgressbar();
}
