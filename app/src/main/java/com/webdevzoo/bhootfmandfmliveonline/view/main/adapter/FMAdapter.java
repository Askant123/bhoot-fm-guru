package com.webdevzoo.bhootfmandfmliveonline.view.main.adapter;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.model.FM;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FMAdapter extends RecyclerView.Adapter<FMAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cover)
        ImageView cover;
        @BindView(R.id.title)
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private MainPresenter mPresenter;

    public FMAdapter(MainPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_fm_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (!(mPresenter.getData().get(position) instanceof FM)) {
            return;
        }
        FM element = (FM) mPresenter.getData().get(position);

        Picasso.with(holder.cover.getContext())
                .load(Uri.parse(element.getImage()))
                .into(holder.cover);
        holder.title.setText(element.getTitle());
        holder.cover.setOnClickListener((view -> mPresenter.setFM(position)));

    }

    @Override
    public int getItemCount() {
        return mPresenter.getData().size();
    }

}

