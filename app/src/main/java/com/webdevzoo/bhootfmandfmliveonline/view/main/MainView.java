package com.webdevzoo.bhootfmandfmliveonline.view.main;

import com.webdevzoo.bhootfmandfmliveonline.view.BaseView;

import java.util.ArrayList;

public interface MainView extends BaseView {
    void onAdsReceived();
    void onDataReceived(ArrayList<Object> data);
    void onDataError();
    void onFMSet(int id);
}
