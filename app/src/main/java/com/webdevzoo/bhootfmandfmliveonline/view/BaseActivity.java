package com.webdevzoo.bhootfmandfmliveonline.view;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.webdevzoo.bhootfmandfmliveonline.R;

import butterknife.BindView;


public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    @BindView(R.id.progress_load)
    ProgressBar progressBar;

    @Override
    public void showProgressbar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        progressBar.setVisibility(View.INVISIBLE);
    }
}
