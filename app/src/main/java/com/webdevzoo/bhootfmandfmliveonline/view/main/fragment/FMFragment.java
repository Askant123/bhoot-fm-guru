package com.webdevzoo.bhootfmandfmliveonline.view.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webdevzoo.bhootfmandfmliveonline.App;
import com.webdevzoo.bhootfmandfmliveonline.R;
import com.webdevzoo.bhootfmandfmliveonline.model.FM;
import com.webdevzoo.bhootfmandfmliveonline.presenter.main.MainPresenter;
import com.webdevzoo.bhootfmandfmliveonline.view.main.adapter.FMAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FMFragment extends MainFragment {

    private Unbinder unbinder;

    @BindView(R.id.content)
    RecyclerView mRecyclerView;

    private FMAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fm, container, false);
        unbinder = ButterKnife.bind(this, view);

        adapter = new FMAdapter(mPresenter);

        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));

        mRecyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void onDataReceived(ArrayList<FM> data) {
        Log.e("On data recieved", "happens");
        adapter.notifyDataSetChanged();
    }
}
