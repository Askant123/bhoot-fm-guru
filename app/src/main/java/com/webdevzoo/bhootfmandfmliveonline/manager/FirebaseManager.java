package com.webdevzoo.bhootfmandfmliveonline.manager;

import com.androidhuman.rxfirebase2.database.RxFirebaseDatabase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.reactivex.Observable;

public class FirebaseManager {

    public Observable<DataSnapshot> getData(String key){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(key);

        return RxFirebaseDatabase.dataChanges(ref);
    }

}
