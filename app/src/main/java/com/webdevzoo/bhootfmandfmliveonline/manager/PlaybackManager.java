package com.webdevzoo.bhootfmandfmliveonline.manager;


import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.webdevzoo.bhootfmandfmliveonline.App;

public class PlaybackManager {

    private SimpleExoPlayer player;

    private Context context;

    public PlaybackManager(App context) {
        this.context = context;
        player = ExoPlayerFactory.newSimpleInstance(context,
                new DefaultTrackSelector());
        isTrackPlaying = false;
    }

    private boolean isTrackPlaying;

    public void initTrack(String url) {
        DataSource.Factory factory = new DefaultDataSourceFactory(context,
                Util.getUserAgent(context, "Bhoot FM and FM live online"));

        MediaSource audioSource = new ExtractorMediaSource(Uri.parse(url),
                factory, Mp3Extractor.FACTORY, null, null);
        LoopingMediaSource loopingMediaSource = new LoopingMediaSource(audioSource);

        player.prepare(loopingMediaSource);
    }

    public void play(){
        isTrackPlaying = true;
        player.setPlayWhenReady(true);
    }

    public void pause(){
        isTrackPlaying = false;
        player.setPlayWhenReady(false);
    }

    public boolean isTrackPlaying(){
        return isTrackPlaying;
    }

    public long getCurrentPosition(){
        Log.e("Current position", String.valueOf(player.getCurrentPosition()));
        return player.getCurrentPosition();
    }

    public long getTrackDuration(){
        Log.e("Duration", String.valueOf(player.getDuration()));
        return player.getDuration();
    }
}
