package com.webdevzoo.bhootfmandfmliveonline.manager;

import java.util.ArrayList;

public class DataManager {

    private ArrayList<Object> data;

    public DataManager() {
        data = new ArrayList<>();
    }

    public ArrayList<Object> getData() {
        return data;
    }

    public void setData(ArrayList<Object> data) {
        this.data = data;
    }
}
